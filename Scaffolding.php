<?php

namespace doctrine\Dashes;

class Scaffolding {
    
    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::__construct as parent_construct;
    }
    
    public function __construct() {
        $this->setAttr('table', $_GET['t']);
        !empty($_GET['pk']) && $this->setAttr('primaryKey', $_GET['pk']);
        $this->deactivate = \defined('\DB_FIELD_DELETE') ? \DB_FIELD_DELETE : false;
        
        call_user_func_array([$this, 'parent_construct'], func_get_args());
    }
    
}