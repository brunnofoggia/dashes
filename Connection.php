<?php

namespace doctrine\Dashes;


/**
 * Connection Trait
 *
 * Connection standard to handle connection configuration list and dbal connections
 *
 * @package     Dashes
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/dashes
 */
trait Connection {

    public $db;

    /** drivers alias list */
    public static $conn_drivers = [
        'mysql' => 'pdo_mysql',
        'sqlsrv' => 'sqlsrv',
    ];

    /** Connection Config List */
    protected static $connConfigList = [];
    
    /** Default Connection */
    protected static $connDefaultConfig = "default";

    /**
     * Set Connection Config List
     * @param array $list database connections
     */
    public static function setConnConfigList($list) {
        foreach ($list as $name => $config) {
            static::setConnConfig($name, $config);
        }
    }

    /**
     * Set Connection Config
     * 
     * @param string $name connection alias
     * @param array $config [user,pass,port,dbname,driver]
     */
    public static function setConnConfig($name, $config) {
        empty($name) && ($name = static::$connDefaultConfig);
        static::$connConfigList[(string) $name] = $config;
        count(static::$connConfigList) === 1 && (static::setDefaultConnConfig($name));
    }

    /**
     * Set Default Connection Config
     * @param string $name connection alias
     */
    public static function setDefaultConnConfig($name) {
        array_key_exists($name, static::$connConfigList) && (static::$connDefaultConfig = $name);
    }

    /**
     * Get Connection Config List
     */
    protected static function getConnConfigList() {
        return self::$connConfigList;
    }

    /**
     * Get Connection Config
     * @param string $name connection alias
     */
    public static function getConnConfig($name) {
        $connConfigList = self::getConnConfigList();
        if (empty($connConfigList)) {
            throw new \Exception('Database config not found');
        }

        $name === 'default' && ($name = static::$connDefaultConfig);
        return array_key_exists($name, $connConfigList) ? $connConfigList[$name] : null;
    }

    /**
     * Connect to database according to parameters received
     * 
     * It can be:
     * 1. Nothing. default connection will be used
     * 2. Connection alias
     * 3. DSN String
     * 4. Connection Config Array
     */
    public function __construct() {
        $args = $this->readArgs(func_get_args());

        if (count($args) === 1) {
            if (is_string($args[0])) {
                $this->db = static::dsnConnect($args[0]);
            } elseif (is_array($args[0])) {
                $this->db = $this->configConnect($args[0]);
            }
        } elseif (count($args) > 1) {
            $this->db = $this->configConnect($args);
        }
    }

    /**
     * Read arguments received to build connection config array
     * @param array $args
     * @return array
     */
    public function readArgs($args) {
        !count($args) && ($args[0] = !empty($this->getAttr('database')) ? $this->getAttr('database') : 'default');
        $config = null;
        if ((count($args) === 1 && is_string($args[0]))) {
            $config = static::getConnConfig($args[0]);
        }

        if ($config !== null) {
            $args = [
                $config['user'],
                (string) @$config['pass'],
                $config['host'],
                (string) @$config['port'],
                (string) @$config['dbname'],
                array_key_exists('driver', $config) ? $config['driver'] : 'mysql',
                @$config['charset'],
            ];
        }

        return $args;
    }

    /**
     * Connect to the database
     * @param array $config
     * @return object connection
     */
    public function configConnect($config) {
        $p = [];
        @list($p['user'], $p['password'], $p['host'], $p['port'], $p['dbname'], $p['driver'], $p['charset']) = $config;
        array_key_exists($p['driver'], static::$conn_drivers) && ($p['driver'] = static::$conn_drivers[$p['driver']]);
        
        $connInstanceId = static::getConnInstanceId($p);
        
        if(!$this->getGlobal($connInstanceId)) {
            $config = new \Doctrine\DBAL\Configuration();
            $conn = \Doctrine\DBAL\DriverManager::getConnection($p, $config);
            $this->setGlobal($connInstanceId,$conn);
        }

        return $this->getGlobal($connInstanceId);
}
    
    /**
     * Creates a unique id to the parameters of a connection
     * @param array $config dbal connection parameters
     * @return string
     */
    public static function getConnInstanceId($config) {
        return 'conn_'.md5(implode(',',$config));
    }

}
