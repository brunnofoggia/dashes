<?php

namespace doctrine\Dashes;

/**
 * Model Trait
 *
 * This is an additional active record pack for Models.
 * It will provide all is needed to read and persist relational data quick and easy
 *
 * @package     Dashes
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/dashes
 */
trait Model {

    use Connection,
        \HBasis {
        Connection::__construct as _conn_construct;
        \HBasis::__construct as _hbasis_construct;
    }
    
    public function __construct() {
        call_user_func_array([$this, '_conn_construct'], func_get_args());
        call_user_func_array([$this, '_hbasis_construct'], func_get_args());
    }
    
    /**
     * Executes Raw Queries
     * @param string $sql
     * @return object
     */
    public function query($sql) {
        $query = $this->db->query($sql);
        return $query;
    }

    /**
     * Run routine under a Transaction
     * @param type $callback
     * @param mixed $parameter [optional] Zero or more parameters to be passed to the callback.
     * @return boolean
     */
    public function transact($callback) {
        $callbackData = array_slice(func_get_args(), 1);
        if($this->getGlobal('transaction_running')) {
            return call_user_func_array($callback, $callbackData);
        }
        
        $this->setGlobal('transaction_running', true);
        $this->db->beginTransaction();
        try {
            $result = call_user_func_array($callback, $callbackData);
            $this->db->commit();
        } catch (\Exception $e) {
            $this->db->rollBack();
            $log = \Crush\Log::get('transaction-error', null, true, true)->clearOldFiles(1, 'd')->add($e->getMessage());
            if(ENVIRONMENT === 'production') {
                $result = false;
                \error_log($e->getMessage(), 0);
            } else {
                throw new \Exception($e->getMessage() . ' Log: ' . $log->getLink(), $e->getCode());
            }
        }
        $this->setGlobal('transaction_running', false);
        return $result;
    }

    /**
     * Runs update on data under conditions
     * @param conditions for update
     * @param data to be updated
     * @access public
     */
    public function _updateAll($conditions, $formattedData) {
        $query = $this->getQueryBuilder();
        $query->update($this->getAttr('table'));
        
        foreach ($formattedData as $key => $val) {
            $query->set($key, $val);
        }
        
        $this->where($query, $conditions);
        $result = $query->execute();
        return $result;
    }

    /**
     * Delete rows
     * @param conditions group of filters
     * @access public
     */
    public function _deleteAll($conditions = []) {
        $query = $this->getQueryBuilder();
        $this->where($query, $conditions);
        $query->delete($this->getAttr('table'))->execute();
        return $query;
    }

    /**
     * Add conditions to query recursively diff arrays from common values
     * @param $query
     * @param $conditions
     */
    public function where($query, $conditions) {
        $namedConditions = $this->exchangeListAliases($conditions);
        foreach ($namedConditions as $field => $value) {
            list($fieldAlias, $fieldOperator) = $this->extractFieldData($field);
            $fieldName = $this->getFieldName($fieldAlias);
            
            !is_int($field) && !empty($fieldName) && ($field = $this->quoteField($fieldName));
            $fieldQuoted = $field;
            
            if (!is_array($value) && $value !== null) {
                $operator = !is_int($field) && !empty($fieldOperator) ? $fieldOperator : '=';
                $criteria = is_int($field) ? ($value) : ($fieldQuoted . $operator . $this->quote($value));
            } else if ($value === null) {
                $criteria = $fieldQuoted . ' IS NULL';
            } else {
                $operator = strtoupper(!empty($fieldOperator) ? $fieldOperator : 'IN');
                $operator === 'NOT' && ($operator .= ' IN');
                $operator === '<>' && ($operator = 'NOT IN');
                $criteria = $fieldQuoted . ' ' . $operator . ' (' . $this->quote($value) . ')';
            }
            
            $query->andWhere($criteria);
        }
        return $query;
    }

    /**
     * Order results
     * @param $query
     * @param $orderby list of fields to be sorted. Describe them like = [field1, field2 desc, field3 asc]
     */
    public function orderby($query, $orderby) {
        !empty($orderby) && is_string($orderby) && ($orderby = explode(',', $orderby));

        if (!empty($orderby)) {
            foreach ($orderby as $value) {
                @list($fieldAlias, $order) = $this->extractFieldData($value);
                $fieldName = $this->getFieldName($fieldAlias);

                $query->addOrderBy($this->quoteField($fieldName), $order);
            }
        }

        return $query;
    }

    /**
     * Returns query builder instance
     */
    public function getQueryBuilder() {
        return $this->db->createQueryBuilder();
    }

    /**
     * Trick to paginate easily with mysql
     * @param $query
     * @param $limit number of rows
     * @param $page use page numbers 0, 1, 2, 3, ...
     */
    public function limit($query, $limit, $page) {
        $query->setFirstResult($page*$limit)
                ->setMaxResults($limit);
    }

    /**
     * Fetch results
     * @param object $query
     * @access public
     */
    public function fetchAll($query) {
        return $query->execute()->fetchAll();
    }

    /**
     * Affected rows
     * @param object $query
     * @access public
     */
    public function affectedRows($query) {
        return $query;
    }

    /**
     * Last Inserted Id
     * @param object $query
     * @access public
     */
    public function lastInsertId($query) {
        return $this->db->lastInsertId();
    }

    /**
     * Quote string value
     * @param string $value
     * @return string
     */
    public function quote($value) {
        if (is_array($value)) {
            $self = $this;
            return implode(',', array_map(function($value) use ($self) {
                        return $self->quote($value);
                    }, $value));
        }

        if (preg_match('/\D/', str_replace('.', '', $value)) || $this->getAttr('quoteAlways')) {
            $value = $this->db->quote($value);
        }
        return $value;
    }

    /**
     * Quote identifier
     * @param string $field
     * @return string
     */
    public function quoteField($field) {
        if (preg_match('/^([A-Za-z0-9-_])+$/', $field)) {
            $field = $this->db->quoteIdentifier($field);
        }
        return $field;
    }

}

define(__NAMESPACE__ . '\BELONGSTO', \HBasis\BELONGSTO);
define(__NAMESPACE__ . '\HASMANY', \HBasis\HASMANY);
define(__NAMESPACE__ . '\HASANDBELONGSTOMANY', \HBasis\HASANDBELONGSTOMANY);
define(__NAMESPACE__ . '\NORELATED', \HBasis\NORELATED);
