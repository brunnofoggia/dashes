<?php

namespace doctrine\Dashes;

/**
 * Model Trait
 *
 * This integrates Models with record control
 *
 * @package     Dashes
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/dashes
 */
trait ControlRecord {
    use \doctrine\Dashes\Model,
            \HiMax\ControlRecord {
        \doctrine\Dashes\Model::__construct as protected parent_construct;
    }
    
    public function __construct() {
        !$this->getAttr('events') && $this->setAttr('events', []);
        $this->setAttr('events', array_merge($this->getAttr('events'), $this->getRecordControlEvents()));
//        $this->events = array_merge($this->getAttr('events'), $this->getRecordControlEvents());
        $this->parent_construct();
    }
}